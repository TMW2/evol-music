TMW2 Music License

Formal DMCA Complains: Please fill the standard form and submit to admin@tmw2.org
The same 30-days rule still apply.

----

GPLv2: Licensed under GNU General Public License version2 or later.
http://www.gnu.org/licenses/gpl-2.0.html
See also the GPL file.

GPLv3: Licensed under GNU General Public License version3 or later.
http://www.gnu.org/licenses/gpl-3.0.html
See also the GPL file.

CC BY-SA 4.0: Licensed under Creative Commons Attribution-ShareAlike 4.0 Unported.
http://creativecommons.org/licenses/by-sa/4.0/
See also the CC BY-SA 4.0 file.

CC0 : Licensed under Creative Commons Zero
http://creativecommons.org/licenses/cc0
Also known as “Public Domain Dedication”

CC BY 3.0: Licensed under Creative Commons Attribution 4.0 International.
http://creativecommons.org/licenses/by/3.0/

CC BY 4.0: Licensed under Creative Commons Attribution 4.0 International.
http://creativecommons.org/licenses/by/4.0/

This copyright notice must be kept intact for use, according to CC BY-SA
compatibility terms.
Where relevant, you must also include a link to https://tmw2.org in your credit.

Where relevant,
You must include a link to http://evolonline.org in your credits along with the author's name.

For Evol Online contributor and license ressources, check the following links:
http://wiki.evolonline.org/contributors
https://www.gitorious.org/evol/clientdata-beta/blobs/master/CONTRIBUTORS
http://wiki.evolonline.org/license
https://www.gitorious.org/evol/evol-music/blobs/master/LICENSE

---------------------------------------------------------------------------------------------------------------------------------

# Folder
    file name (author name)                                 (License)           (Contributors' Nick or Source)
    Original music names (if any) can be found on metatags

# music
    3b5.ogg (Brandon Morris)                                (CC 0)              (https://opengameart.org/)
    8bit_the_hero.ogg (Ryan Burks)                          (CC 0)              (https://opengameart.org/)
    academy_bells.ogg                                       (GPL 3.0/CC-BYSA 3) (Nelson James Gatlin/Source Of Tales)
    adonthell.ogg (ZhayTee)                                 (GPL 3.0)           (Adonthell: Waste's Edge OST)
    Arabesque.ogg (Chris Janowiak)                          (GPL 3.0/CC-BY 3.0) (https://opengameart.org/content/arabesque)
    bartk_adventure.ogg (Bart Kelsey)                       (CC BY 4.0)         (Socapex/Evol Online)
    caketown.ogg (Matthew Pablo)                            (CC-BY-SA-3.0)      (Saulc)
    dance_monster.ogg (Kevin MacLeod)                       (CC BY 3.0)         (http://incompetech.com/m/c/royalty-free/)
    dariunas_forest.ogg                                     (GPLv2)             (Aasif Chaudry? The Mana World)
    dragon_and_toast.ogg (Kevin MacLeod)                    (CC BY 3.0)         (http://incompetech.com/m/c/royalty-free/)
    Deep_Cave.ogg (YD)                                      (CC 0)              (https://opengameart.org/) (Original name: WeltHerrscherer Theme 1)
    eric_matyas_ghouls.ogg                                  (CC BY 4.0)         (Riqqi/Evol Online)
    eric_matyas_surreal.ogg                                 (CC BY 4.0)         (Riqqi/Evol Online)
    forest-sprint.ogg (Jason Lavallée)                      (GPLv2/CC BY SA 4)  (SuperTux 2)
    icecave.ogg (aquileia/iceiceice)                        (GPLv2)             (Battle For Wesnoth Addons - UMC Music Book 11)
    let_the_battles_begin.ogg (northivanastan)              (CC 0)              (https://opengameart.org/)
    magick - real.ogg (Aasif Chaudry)                       (GPLv2)             (The Mana World)
    Misty_Shrine.ogg (ORMIX)                                (CC BY 3.0)         (https://www.newgrounds.com/audio/listen/685621)
    mythica.ogg (congusbongus)                              (CC 0)              (https://opengameart.org/)
    misuse.ogg (Kevin MacLeod)                              (CC BY 3.0)         (http://incompetech.com/m/c/royalty-free/)
    mvrasseli_nochains.ogg (mvrasseli)                      (CC BY 3.0)         (https://opengameart.org/)
    New_Woodlands.ogg (Écrivan)                             (CC 0)              (https://opengameart.org/) (Original name: Goodbye to Friends)
    peace.ogg (Doulos)                                      (GPLv2)             (The Mana World)
    peace2.ogg (Aasif Chaudry)                              (GPLv2)             (The Mana World)
    peace3.ogg (Saturn)                                     (GPLv2)             (The Mana World - CR 1)
    PerituneMaterial_Minstrel2_Harp.ogg                     (CC-BY 4.0)         (https://peritune.com/blog/2019/05/24/minstrel2/)
    Ruins.ogg (Aphyllix)                                    (CC BY 3.0)         (https://www.newgrounds.com/audio/listen/878890)
    school_of_quirks.ogg (Zander Noriega)                   (CC-BY 3.0)         (-)
    sidequests.ogg (Komiku)                                 (CC 0)              (https://freemusicarchive.org/)
    snowvillage.ogg (Aasif Chaudry)                         (CC BY 3.0)         (TMW.org)  2008
    steam.ogg (Chicka-Maria)                                (CC BY 3.0)         (jesusalva)
    tws_birds_in_the_sunrise.ogg (Terraws)                  (CC BY 2.0)         (https://www.jamendo.com/artist/348491/terraws)
    tws_green_island.ogg (Terraws)                          (CC BY 2.0)         (https://www.jamendo.com/artist/348491/terraws)
    Unforgiving Lands.ogg (HorrorPen)                       (CC BY 3.0)         (https://opengameart.org/)
    valkyries.ogg (Richard Wagner)                          (Public Domain)     (American Symphony Orchestra, 1921 - Ride of Valkyries)
    water_prelude.ogg (Kevin MacLeod)                       (CC BY 3.0)         (http://incompetech.com/m/c/royalty-free/)
    woodland_fantasy.ogg (Matthew Pablo)                    (CC-BY 3.0)         (Saulc)

# music medleys
    sail_away.ogg (*)
        + Up to 1m21s:
            Tarhensis Theme (Tobias Escher)                 (GPLv2)            (https://opengameart.org/)
        + From 1m22s:
            Sail Away! (Jose. S & Dakota. D)                (GPLv2)            (The Mana World)
    tmw_adventure.ogg (*)
        + Up to 1m00s:
            Explorers Melody (Jose. S & Dakota. D)          (GPLv2)            (The Mana World)
        + From 1m01s:
            Faith (Unknown Author)                          (GPLv2)            (The Mana World)


